# dpathst - little path navigator

include config.mk

default: install

clean:
	rm -f dpathst-${VERSION}.tar.xz

dist: clean
	mkdir -p dpathst-${VERSION}
	cp -R LICENSE README Makefile config.mk dpathst.1 \
		dpathst \
		dpathst-${VERSION}
	tar -cf dpathst-${VERSION}.tar dpathst-${VERSION}
	xz -9 -T0 dpathst-${VERSION}.tar
	rm -rf dpathst-${VERSION}

install:
	cp -f dpathst ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/dpathst
	mkdir -p  ${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < dpathst.1 > ${MANPREFIX}/man1/dpathst.1

uninstall:
	rm -r ${PREFIX}/bin/dpathst ${MANPREFIX}/man1/dpathst.1

.PHONY: clean dist install uninstall
